import { Component } from 'react';
import CardList from './components/card-list/card-list.component';
import './App.css';
// import AttackTitan from './assets/img/attack.png';
// import BeastTitan from './assets/img/beast.png';
import SearchBox from './components/search-box/search-box.component';

class App extends Component {
  constructor() {
    super();

    this.state = {
      titans: [
        {
          name: 'Bokuko',
          key: 1,
          image: require('./assets/img/Bokuto.gif').default,
          human: 'Side Attacker',
        },
        {
          name: 'Hinata Spikes',
          key: 2,
          image: require('./assets/img/hinata.gif').default,
          human: 'Middle Attacker',
        },
        {
          name: 'Hinata attacks',
          key: 3,
          image: require('./assets/img/hinata_spikes.gif').default,
          human: 'Middle Attacker',
        },
        {
          name: 'kageyama_Tobio',
          key: 4,
          image: require('./assets/img/kageyama_Tobio.gif').default,
          human: 'Setter',
        },
        {
          name: 'Kuroo',
          key: 5,
          image: require('./assets/img/kuroo.gif').default,
          human: 'Side Attacker',
        },
        {
          name: 'Mad-Dog',
          key: 6,
          image: require('./assets/img/mad_DOG.gif').default,
          human: 'Opposite attack',
        },
        {
          name: 'Nishinoya',
          key: 7,
          image: require('./assets/img/nishinoya.gif').default,
          human: 'Libero',
        },
        {
          name: 'Oikawa',
          key: 8,
          image: require('./assets/img/oikawa.gif').default,
          human: 'Setter',
        },
        {
          name: 'Tendo',
          key: 9,
          image: require('./assets/img/tendo.gif').default,
          human: 'Middle Blocker',
        },
        {
          name: 'Tsukishima',
          key: 10,
          image: require('./assets/img/tsukishima.gif').default,
          human: 'Middle Blocker',
        },
        {
          name: 'UKAI KEISHIN',
          key: 11,
          image: require('./assets/img/UKAI KEISHIN.gif').default,
          human: 'Head Coach',
        },
        {
          name: 'Ushijima wakatoshi',
          key: 12,
          image: require('./assets/img/Ushijima wakatoshi.gif').default,
          human: 'Side Attacker',
        },
        {
          name: 'Yachi',
          key: 13,
          image: require('./assets/img/Yachi.gif').default,
          human: 'Assistant Coach',
        },
      ],
      searchField: '',
    };

    // this.handleChange = this.handleChange.bind(this);
  }

  // componentDidMount() {
  //   fetch('https://jsonplaceholder.typicode.com/users')
  //     .then(response => response.json())
  //     .then(users => this.setState({ titans: users }));
  // }

  handleChange = (e) => {
    this.setState({ searchField: e.target.value });
  };

  render() {
    const { titans, searchField } = this.state;
    const filteredTitans = titans.filter(titan =>
      titan.name.toLowerCase().includes(searchField.toLowerCase())
    );
    return (
      <div className="App">
        <h1>Haikyuu Anima</h1>
        <SearchBox
          placeholder="Search volleyballer"
          handleChange={e => this.handleChange(e)}
        />
        <CardList titans={filteredTitans} />
      </div>
    );
  }
}

export default App;
 