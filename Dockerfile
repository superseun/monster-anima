FROM node:10.15.0

RUN mkdir -p /codetest

WORKDIR /codetest

COPY package.json .

RUN npm install

COPY . /codetest

EXPOSE 80

CMD ["npm", "start"]

FROM node:latest

# Create app directory
#RUN mkdir -p /codetest

#WORKDIR
#WORKDIR /codetest

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
#COPY package*.json /

#RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
#COPY . /codetest

#EXPOSE 80
#CMD [ "node", "start" ]